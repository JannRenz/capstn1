﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farm : MonoBehaviour
{
    public enum STATE
    {
        FREE,
        PLANTING,
        HARVEST
    };

    STATE state;
    public Plants selectedPlant;

    public static string plantName = "none";
    public float timer = 1f;
    public bool hasPlant = false;
    public bool canHarvest = false;
    public float plantTime;
    public float showHarvestTime;

    public GameObject seed;
    public GameObject halfPlant;
    public GameObject fullPlant;

    public CurrencyScript currency;

    void Start()
    {
        currency = GameObject.Find("CurrencyManager").GetComponent<CurrencyScript>();
        seed.SetActive(false);
        halfPlant.SetActive(false);
        fullPlant.SetActive(false);
        //state = STATE.FREE;
    }
    void Update()
    {
        if (selectedPlant != null)
        {
            showHarvestTime = selectedPlant.harvestTime;
        }
        
        switch (state)
        {
            case STATE.FREE:
                Free();
                break;
            case STATE.PLANTING:
                Planting();
                break;
            case STATE.HARVEST:
                Harvesting();
                break;
        }
    }
    void Free()
    {
        //this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        seed.SetActive(false);
        halfPlant.SetActive(false);
        fullPlant.SetActive(false);
        selectedPlant = null;
        hasPlant = false;
        canHarvest = false;
    }
    void Planting()
    {
        seed.SetActive(true);
        halfPlant.SetActive(false);
        fullPlant.SetActive(false);
        if (hasPlant && selectedPlant != null)
        {
            plantTime -= Time.deltaTime;
            if (plantTime <= selectedPlant.StartingharvestTime/2)
            {
                seed.SetActive(false);
                halfPlant.SetActive(true);
                fullPlant.SetActive(false);
                Debug.Log("half");
            }
            if (plantTime <= 0)
            {
                plantTime = 0;
                canHarvest = true;
                state = STATE.HARVEST;
            }
        }
    }
    void Harvesting()
    {
        seed.SetActive(false);
        halfPlant.SetActive(false);
        fullPlant.SetActive(true);
        //this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
    }
    void OnMouseDown()
    {
        //Debug.Log("Clicked");
        if (plantName == "Carrot" && !hasPlant)
        {
            selectedPlant = GameObject.Find(plantName).GetComponent<Plants>();
            plantTime = selectedPlant.harvestTime;
            plantName = "none";
            hasPlant = true;
            state = STATE.PLANTING;
        }
        if (plantName == "Cabbage" && !hasPlant)
        {
            selectedPlant = GameObject.Find(plantName).GetComponent<Plants>();
            plantTime = selectedPlant.harvestTime;
            plantName = "none";
            hasPlant = true;
            state = STATE.PLANTING;
        }
        if (plantName == "Tomato" && !hasPlant)
        {
            selectedPlant = GameObject.Find(plantName).GetComponent<Plants>();
            plantTime = selectedPlant.harvestTime;
            plantName = "none";
            hasPlant = true;
            state = STATE.PLANTING;
        }
        if (plantName == "Potato" && !hasPlant)
        {
            selectedPlant = GameObject.Find(plantName).GetComponent<Plants>();
            plantTime = selectedPlant.harvestTime;
            plantName = "none";
            hasPlant = true;
            state = STATE.PLANTING;
        }
        if (plantName == "Wheat" && !hasPlant)
        {
            selectedPlant = GameObject.Find(plantName).GetComponent<Plants>();
            plantTime = selectedPlant.harvestTime;
            plantName = "none";
            hasPlant = true;
            state = STATE.PLANTING;
        }
        else if (selectedPlant && hasPlant && canHarvest)
        {
            currency.foodRate += selectedPlant.foodGiven;
            state = STATE.FREE;
        }
    }
}
