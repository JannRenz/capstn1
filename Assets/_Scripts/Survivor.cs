﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Survivor : MonoBehaviour
{
    public List<Sprite>villagerSprite;
    public SpriteRenderer spriteRenderer;
    public bool isTaken;
    public Vector3 currentPos;
    public Vector3 previousPos;
	// Use this for initialization
	void Start ()
    {
        currentPos = transform.position;
        previousPos = currentPos;
        spriteRenderer.sprite = villagerSprite[Random.Range(0,villagerSprite.Count)];
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = currentPos;
	}
}
