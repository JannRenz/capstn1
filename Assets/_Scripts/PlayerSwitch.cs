﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwitch : MonoBehaviour
{

    public bool isPlanting;
    public bool isShooting;

    Animator animator;
	// Use this for initialization

	void Start ()
    {
        isPlanting = true;
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            isShooting = true;
            isPlanting = false;
            animator.SetBool("isShooting",true);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            isShooting = false;
            isPlanting = true;
            animator.SetBool("isShooting", false);
        }
	}
}
