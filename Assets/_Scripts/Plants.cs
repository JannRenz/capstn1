﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plants : MonoBehaviour
{
    public new string name;
    public float nutrition;
    public float StartingharvestTime;
    public float harvestTime;

    public float foodGiven;
    public float waterCost;
    public GameObject farm;

    public bool wasPressed;

    void Start()
    {
        StartingharvestTime = harvestTime;
    }
    public void SelectPlant()
    {
        
        if (gameObject.name == "Carrot")
        {
            Farm.plantName = "Carrot";
            wasPressed = true;
        }
        if (gameObject.name == "Cabbage")
        {
            Farm.plantName = "Cabbage";
            wasPressed = true;
        }
        if (gameObject.name == "Tomato")
        {
            Farm.plantName = "Tomato";
            wasPressed = true;
        }
        if (gameObject.name == "Potato")
        {
            Farm.plantName = "Potato";
            wasPressed = true;
        }
        if (gameObject.name == "Wheat")
        {
            Farm.plantName = "Wheat";
            wasPressed = true;
        }
    }

}
