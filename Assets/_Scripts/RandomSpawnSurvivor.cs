﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnSurvivor : MonoBehaviour
{

    public List<Transform> spawnPoint;
    public GameObject survivors;
    public bool hasSpawn;
    private int randNum;
	// Use this for initialization
	void Start ()
    {
        hasSpawn = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        randNum = Random.Range(0,spawnPoint.Count);
        if (!hasSpawn)
        {
            for (int i = 0; i <= spawnPoint.Count; i++)
            {
                if (!spawnPoint[randNum].GetComponent<SpawnPointScript>().hasVillager)
                {
                    Instantiate(survivors, spawnPoint[randNum].transform.position, Quaternion.identity);
                    spawnPoint[randNum].GetComponent<SpawnPointScript>().hasVillager = true;
                }
                else
                {
                    return;
                }
            }

            hasSpawn = true;
        }
	}
}
