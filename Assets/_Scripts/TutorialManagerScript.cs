﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialManagerScript : MonoBehaviour
{
    [HideInInspector] public Farm farmScript;
    [HideInInspector] public Plants plantScript;
    public TextMeshProUGUI tooltipText;
    public bool hasInteracted; //with farmer
    //public bool isInScavengingArea; //first time arriving
    public bool hasPlanted; //at least one seed
    //public bool hasRescued; //end tutorial
    public bool hasClicked;
    public GameObject arrowIndicator;
    public GameObject fenceBlocker;
    public GameObject buttonHighlighter;
    public GameObject cropHighlighter;
    private bool tutorialDone;

    void Start ()
    {
        StartCoroutine(CallFunctionWithDelay(0.1f));
        hasInteracted = true; //temporary while interact is not yet implemented
        fenceBlocker.gameObject.SetActive(true);
        buttonHighlighter.gameObject.SetActive(false);
        cropHighlighter.gameObject.SetActive(false);
        farmScript = GameObject.FindGameObjectWithTag("Farm").GetComponent<Farm>();
        plantScript = GameObject.FindGameObjectWithTag("Plants").GetComponent<Plants>();
        tutorialDone = false;
        hasPlanted = false;
        hasClicked = false;
    }

    void Update()
    {
        if(hasInteracted)
        {
            if(Input.GetMouseButtonDown(0))
            {
                hasClicked = true;
            }
        }
    }

    void TutorialFunction()
    {
        if (hasPlanted)
        {
            StartCoroutine(DisplayRollingText("You should try looking for loot outside the village. Just be back before dark!"));
            arrowIndicator.gameObject.SetActive(true);
            Debug.Log("hasPlanted");
        }

        if (hasInteracted)
        {
            StartCoroutine(DisplayRollingText("Oh good you're back. While you're here try planting some seeds why dontcha?"));
            buttonHighlighter.gameObject.SetActive(true);
            Debug.Log("hasInteracted");
            if (hasClicked /*&& plantScript.wasPressed == true*/)
            {
                Debug.Log("Clicked");
                buttonHighlighter.gameObject.SetActive(false);
                cropHighlighter.gameObject.SetActive(true);
                if(farmScript.hasPlant == true)
                {
                    hasPlanted = true;
                    cropHighlighter.gameObject.SetActive(false);
                    hasInteracted = true;
                }
            }
        }
    }

    IEnumerator DisplayRollingText(string text)
    {
        tooltipText.text = "";
        foreach (char letter in text)
        {
            tooltipText.text += letter;
            yield return new WaitForEndOfFrame();
        }
        //hasInteracted = false;
    }

    IEnumerator CallFunctionWithDelay(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        TutorialFunction();
    }

    void PauseTime()
    {
        Time.timeScale = 0;
    }
}
