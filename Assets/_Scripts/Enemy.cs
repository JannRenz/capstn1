﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float hp;
    public float maxhp;
    public float range;
    public LayerMask target;
    public EnemyMove enemyMove;

    public Animation anim;
    
	// Use this for initialization
	void Start ()
    {
        hp = maxhp;
        enemyMove = this.gameObject.GetComponent<EnemyMove>();
        anim = this.gameObject.GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        DetectEnemy();

        if (hp <= 0)
        {
            Dead();
        }
	}

    void Dead()
    {
        Destroy(this.gameObject);
    }
    void DetectEnemy()
    {
        Collider2D enemies = Physics2D.OverlapCircle(this.transform.position, range, target);
        if (enemies != null)
        {
            enemyMove.target = enemies.gameObject.transform;
            //Debug.Log("Player Detected");
        }
        else
        {
            enemyMove.target = GameObject.FindGameObjectWithTag("Farm").transform;
            //Debug.Log("Farm Located");
        }

    }
}
