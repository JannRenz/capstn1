﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

    private float mouseX;
    private float mouseY;

    private bool VerticalRotationEnabled = true;
    private float VerticalRotationMin = 0f;
    private float VerticalRotationMax = 65f;

    private Vector3 mouseOrigin;
    private bool isRotating;
    public float turnSpeed;

	void LateUpdate ()
    {

        if (Input.GetMouseButtonDown(1))
        {
            mouseOrigin = Input.mousePosition;
            isRotating = true;
        }

        if (Input.GetMouseButtonUp(1))
        {
            isRotating = false;
        }
            
        if (isRotating)
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

            transform.RotateAround(transform.position, transform.right, -pos.y * turnSpeed);
            transform.RotateAround(transform.position, Vector3.up, pos.x * turnSpeed);
        }
	}
}
