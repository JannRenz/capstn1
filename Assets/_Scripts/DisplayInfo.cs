﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DisplayInfo : MonoBehaviour
{


    public GameObject panel;
    Vector3 panelTrans;
    public float offX;
    public float offY;
    public TextMeshProUGUI plantName;
    public TextMeshProUGUI waterCost;
    public TextMeshProUGUI foodGiven;
    public TextMeshProUGUI harvestTime;

	void Start ()
    {
        panelTrans = panel.transform.position;
	}
	
	void Update ()
    {
		
	}
    public void Display(Plants plant)
    {
        panel.SetActive(true);
        panel.GetComponent<RectTransform>().position = new Vector3(plant.gameObject.transform.position.x - offX, plant.gameObject.transform.position.y + offY, 0);//new Vector3(plant.gameObject.transform.position.x - offX, plant.gameObject.transform.position.y - offY, 0);
        plantName.text = plant.name.ToString();
        waterCost.text = "Water Cost: " + plant.waterCost.ToString("F0");
        foodGiven.text = "Food Given: " + plant.foodGiven.ToString("F0");
        harvestTime.text = "Harvest Time: " + plant.harvestTime.ToString("F0");

    }
    public void Hide()
    {
        panel.SetActive(false);
    }
}
