﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PickupSurvivor : MonoBehaviour
{
    [SerializeField]
    List<Survivor> survivorPos = new List<Survivor>();
    public GameObject survivorPrefab;
    ScreenFader screenFader;
    Vector3 playerPos;
    Vector3 previousPlayerPos;
    bool hasTaken;
    bool isMoving;
    public GameManager gameManager;
    public CurrencyScript currency;
    public float offSetX;
    public float offSetY;
    // Use this for initialization
    void Start()
    {
        playerPos = transform.position;
        previousPlayerPos = playerPos;
        screenFader = GetComponent<ScreenFader>();
        gameManager = gameManager.GetComponent<GameManager>();

    }

    // Update is called once per frame
    void Update ()
    {
        if (gameManager.gameState != STATES.SCAVENGING)
            return;
        else 
            Debug.Log("Scavenging state");
         Scavenge();
        #region    
        /*
          if(hasTaken == true)
          {
              GameObject tempSurvivor = (GameObject)Instantiate(survivorPrefab, transform.position, Quaternion.identity);
              survivorPos.Insert(0, tempSurvivor.transform);
              hasTaken = false;
          }
          else if (survivorPos.Count > 0)
          {
              survivorPos.Last().position = playerPos; //Move gO to where the player was
              survivorPos.Insert(0, survivorPos.Last());
              survivorPos.RemoveAt(survivorPos.Count - 1);//Add to front of list remove from back
          }
          */
        #endregion
    }

    void Scavenge()
    {
        if (playerPos != transform.position)
        {
            if (transform.position.x - playerPos.x > 0)
                offSetX = -0.5f;
            else if (transform.position.x - playerPos.x < 0)
                offSetX = 0.5f;
            else if (transform.position.x == playerPos.x)
                offSetX = 0.0f;
            if (transform.position.y - playerPos.y > 0)
                offSetY = -0.5f;
            else if (transform.position.y - playerPos.y < 0)
                offSetY = 0.5f;
            else if (transform.position.y == playerPos.y)
                offSetY = 0.0f;
            isMoving = true;
            previousPlayerPos = playerPos;
            playerPos = transform.position;
        }
        else
            isMoving = false;

        if (survivorPos.Count > 0 && isMoving)
        {
            for (int i = 0; i < survivorPos.Count; i++)
            {
                if (i == 0 && survivorPos.Count != 0)
                {
                    survivorPos[i].previousPos = survivorPos[i].transform.position;
                    survivorPos[i].currentPos = new Vector2(previousPlayerPos.x + offSetX, previousPlayerPos.y + offSetY);
                }
                else
                {
                    survivorPos[i].previousPos = survivorPos[i].transform.position;
                    survivorPos[i].currentPos = new Vector2(survivorPos[i - 1].previousPos.x + offSetX, survivorPos[i - 1].previousPos.y + offSetY);
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (gameManager.gameState != STATES.SCAVENGING)
            return;
        else
        //Debug.Log("Scavenging state");
        if (other.gameObject.tag == "Survivor" && other.gameObject.GetComponent<Survivor>() != null)
        {
            if (!other.gameObject.GetComponent<Survivor>().isTaken)
            {
                survivorPos.Add(other.gameObject.GetComponent<Survivor>());
                other.gameObject.GetComponent<Survivor>().isTaken = true;
            }
            #region
            //hasTaken = true;

            //Destroy(other.gameObject);
            //other.gameObject.transform.position = survivorPos.Last().position;
            //other.gameObject.SetActive(false);
            #endregion
        }
        else if (other.gameObject.tag == "Village Entrance")
        {
            if (survivorPos.Count != 0)
            {
                for(int i = 0; i <= survivorPos.Count; i++)
                {
                    currency.populationRate += 1;
                    //survivorPos[i].gameObject.SetActive(false);
                    
                    Destroy(survivorPos[i+1].gameObject);
                }
            }
            //screenFader.FadeToBlack();
        }
    }
}
