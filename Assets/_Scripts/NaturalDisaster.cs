﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaturalDisaster : MonoBehaviour
{
    int randomNumber;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        RollNumber();
        StartCoroutine(TriggerDisaster());
	}

    void RollNumber()
    {
        randomNumber = Random.Range(1, 100);
        //print(randomNumber);
        if (randomNumber == 10)
        {
            //print("Trigger Earthquake");
        }
    }

    IEnumerator TriggerDisaster()
    {
        yield return new WaitForSeconds(0);
    }
}
