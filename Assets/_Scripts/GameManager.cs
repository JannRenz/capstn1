﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum STATES
{
    FARMING,
    SCAVENGING
}


public class GameManager : MonoBehaviour
{
    public STATES gameState;

    void Start ()
    {
        gameState = STATES.SCAVENGING;
    }

    public void switchState(STATES state)
    {   
        switch (state)
        {
            case STATES.FARMING:
                gameState = STATES.FARMING;
                break;
            case STATES.SCAVENGING:
                gameState = STATES.SCAVENGING;
                break;
        }
    }

}
