﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenFader : MonoBehaviour
{
    //[HideInInspector]
    public Image image;
    Color color;
    float fadeSpeed = 4.5f;
    private bool SceneStarting = true;

    public void Start()
    {
       // image = GetComponent<Image>();
        color = image.color;
        image.gameObject.SetActive(true);
    }

    public void Update()
    {
        if (Input.GetKey(KeyCode.K))
            FadeToBlack();
        else if (Input.GetKey(KeyCode.L))
            FadeToClear();

        if (SceneStarting)
            StartScene();
    }

    public void FadeToClear()
    {
        image.color = Color.Lerp(image.color, Color.clear, fadeSpeed * Time.deltaTime);
        //image.color = Vector4.Lerp(image.color, Color.clear, fadeSpeed * Time.deltaTime);
    }

    public void FadeToBlack()
    {
        image.color = Color.Lerp(image.color, Color.black, fadeSpeed * Time.deltaTime);
        //image.color = Vector4.Lerp(image.color, Color.black, fadeSpeed * Time.deltaTime);
    }

    void StartScene()
    {
        //Debug.Log(image.color.a);
        FadeToClear();
        //if (image.color.a <= 0.05)
        //{
        //    image.color = Color.clear;
        //    image.enabled = false;
        //    SceneStarting = false;
        //    Debug.Log(image.color.a);
        //}
    }

    public void EndScene()
    {
        image.enabled = true;
        FadeToBlack();
    }
}
