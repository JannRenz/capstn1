﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockPlants : MonoBehaviour
{

    public ProgressionBar progressBar;
    public List<Button> buttons;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (progressBar.progress >= 5)
        {
            buttons[2].gameObject.SetActive(true);
        }
        if (progressBar.progress >= 15)
        {
            buttons[3].gameObject.SetActive(true);
        }
        if (progressBar.progress >= 30)
        {
            buttons[4].gameObject.SetActive(true);
        }
    }
}
