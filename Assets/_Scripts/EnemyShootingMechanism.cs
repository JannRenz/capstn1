﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootingMechanism : MonoBehaviour {

    public float fireRate = 0;
    public LayerMask whatToHit;

    public Transform BulletTrailPrefab;
    float timeToSpawnEffect = 0;
    public float effectSpawnRate = 10;

    float timeToFire = 0;
    Transform firePoint;
    Enemy self;
    EnemyMove selfMove;

    // Use this for initialization
    void Start ()
    {
        self = this.gameObject.GetComponent<Enemy>();
        selfMove = this.gameObject.GetComponent<EnemyMove>();
        firePoint = GameObject.Find("GunPoint1").transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (selfMove.target != GameObject.FindGameObjectWithTag("Farm").transform)
        {
            if (fireRate == 0)
            {

                Shoot();
            }
            else
            {
                if (Time.time > timeToFire)
                {
                    timeToFire = Time.time + 1 / fireRate;
                }
            }
            
        }
        
    }
    void Shoot()
    {
        if (Time.time >= timeToSpawnEffect)
        {
            Effect();
            timeToSpawnEffect = Time.time + 1 / effectSpawnRate;
        }   
        
    }
    void Effect()
    {
        Instantiate(BulletTrailPrefab, firePoint.transform.position, firePoint.rotation);
    }
}
