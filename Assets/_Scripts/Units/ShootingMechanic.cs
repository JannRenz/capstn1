﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingMechanic : MonoBehaviour
{
    /// <summary>
    /// PublicVariable
    /// privateVariable
    /// Group private and public variables
    /// use quaternion for rotating
    /// if on 2D plane always make sure the Z axes are always equal when finding directions
    /// PUT ALL MOVEMENT CHANGES ESPECIALLY WHEN YOU'RE CHANGING TRANSFORM POSITION IN FIXED UPDATE!!!
    /// </summary>
    public float FireRate = 0;
    public LayerMask WhatToHit;
    public Transform BulletTrailPrefab;
    public float EffectSpawnRate = 10;

    float timeToSpawnEffect = 0;
    float timeToFire = 0;
 //   Transform firePoint;

    void Update()
    {
       // firePoint = GameObject.Find("GunPoint").transform;     // WAG KAYO MAG FIND OBJECT SA UPDATE HOY
        if (FireRate == 0)
        {
            if (Input.GetMouseButtonDown(0))
                Shoot();
        }
        else
        {
            if (Input.GetMouseButtonDown(0) && Time.time > timeToFire)
                timeToFire = Time.time + 1 / FireRate;
        }
    }
    void Shoot()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 dir = new Vector3(mousePosition.x, mousePosition.y, transform.position.z) - transform.position;
        dir.Normalize();

        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 100);
        if (Time.time >= timeToSpawnEffect && hit.collider != null && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())  //basically dont fire if cursor is on UI yung mahabang code
        {
            if (hit.collider)
            {
                Transform temp = Instantiate(BulletTrailPrefab, transform.position, Quaternion.identity);
                temp.GetComponent<Bullet>().direction = dir;
                temp.rotation = Quaternion.LookRotation(dir);
                timeToSpawnEffect = Time.time + 1 / EffectSpawnRate;
            }
        }
        #region old code

        /*
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
      
        Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
        RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 100, whatToHit);
        if (Time.time >= timeToSpawnEffect && hit.collider != null)
        {
            if (hit.collider)
            {
                Effect();
                timeToSpawnEffect = Time.time + 1 / effectSpawnRate;
            }
            //Debug.Log(hit.collider.name);
            
        }
        */
        #endregion
    }
    void Effect()
    {
        #region old code
        /*
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Transform temp =  Instantiate(BulletTrailPrefab, firePoint.localPosition, Quaternion.identity);
        temp.LookAt(mousePosition);
        temp.GetComponent<Bullet>().SetupBullet(firePoint.localPosition, mousePosition);*/
        #endregion
    }
}
