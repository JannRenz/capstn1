﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public float lifetime = 1;
    public Vector3 direction;

    void FixedUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
        Destroy(this.gameObject, lifetime);
    }
}
