﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitAI : MonoBehaviour
{
    public enum STATE
    {
        CHARGE,
        ATTACK
    }

    public STATE state;
    public Unit unit;
    public UnitDetection unitDetection;
    public Animator anim;
    public GameObject EnemyBuilding;
    public NavMeshAgent agent;
    public string TargetBuilding;

	void Start ()
    {
        state = STATE.CHARGE;
        unit = this.gameObject.GetComponent<Unit>();
        unitDetection = this.GetComponentInChildren<UnitDetection>();
        anim = this.gameObject.GetComponent<Animator>();
        agent = this.gameObject.GetComponent<NavMeshAgent>();
        EnemyBuilding = GameObject.FindGameObjectWithTag(TargetBuilding);
	}
	
	void Update ()
    {
        switch (state)
        {
            case STATE.CHARGE:
                Charge();
                break;
            case STATE.ATTACK:
                Attack();
                break;
        }	
	}

    void Charge()
    {
        anim.enabled = false;
        agent.Resume();
        //anim.SetBool("isAttacking", false);
        //agent.SetDestination(EnemyBuilding.transform.position);
        //this.gameObject.transform.Translate(0, 0, 0.1f * unit.mvmspd);
        if (unitDetection.enemy != null)
        {
            state = STATE.ATTACK;
        }
        //unit.DetectEnemy();
    }

    void Attack()
    {
        agent.Stop();
        anim.enabled = true;
        anim.SetBool("isAttacking",true);
        if (unitDetection.enemy == null)
        {
            state = STATE.CHARGE;
        }
    }
}
