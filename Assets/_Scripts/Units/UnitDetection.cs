﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDetection : MonoBehaviour
{

    public GameObject enemy;
    public string enemyTag;
    public string targetCastle;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == enemyTag || other.gameObject.tag == targetCastle)
        {
            enemy = other.gameObject;
            Debug.Log(enemy);   
        }
    }
}
