﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    Animator animator;
    public float speed;
    void Start()
    {
        animator = GetComponent<Animator>();
    }
	void FixedUpdate ()
    {
        Vector2 pos = this.transform.position;

        if (Input.GetKey(KeyCode.W))
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", 1);
            animator.SetFloat("Xvalue", 0);
            //this.transform.Translate(Vector2.up * speed * Time.deltaTime);
            pos.y += speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", -1);
            animator.SetFloat("Xvalue", 0);
            //this.transform.Translate(Vector2.down * speed * Time.deltaTime);
            pos.y -= speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", 0);
            animator.SetFloat("Xvalue", -1);
            //this.transform.Translate(Vector2.left * speed * Time.deltaTime);
            pos.x -= speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", 0);
            animator.SetFloat("Xvalue", 1);
            //this.transform.Translate(Vector2.right * speed * Time.deltaTime);
            pos.x += speed * Time.deltaTime;
        }
        else
        {
            animator.SetBool("isWalking", false);
        }
        transform.position = pos;
    }
}
