﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

    public Transform target;
    public int speed;
    public float dirNum;
    Animator animator;
    // Use this for initialization
    void Start ()   
    {
        target = GameObject.FindGameObjectWithTag("Farm").transform;
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position,target.position, speed * Time.deltaTime);

        Vector3 heading = target.position - transform.position;
        dirNum = AngleDir(transform.forward, heading, transform.up);
    }

    float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0f)
        {
            //animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", 0);
            animator.SetFloat("Xvalue", 1);
            return 1f;
        }
        else if (dir < 0f)
        {
            //animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", 0);
            animator.SetFloat("Xvalue", -1);
            return -1f;
        }
        else
        {
            return 0f;
        }
    }
}
