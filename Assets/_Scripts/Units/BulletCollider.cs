﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollider : MonoBehaviour
{
    public int damage;

    void Start()
    {
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
    }

    void Update()
    {
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "EnemyUnit")
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            Debug.Log("hit");
            enemy.anim.Play();
            enemy.hp -= damage;
            //Debug.Log(enemy.hp);
            Destroy(gameObject);
        }
    }
}
