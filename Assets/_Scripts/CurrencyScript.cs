﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CurrencyScript : MonoBehaviour
{
    private static CurrencyScript instance;

    public static CurrencyScript Instance
    {
        get
        {
            if(!instance)
            {
                instance = GameObject.FindObjectOfType<CurrencyScript>();
                if(!instance)
                {
                    GameObject newInstance = new GameObject("CurrencyManager");
                    instance = newInstance.AddComponent<CurrencyScript>();
                }
            }
            return instance;
        }
    }

    #region

    public TextMeshProUGUI FoodText;
    public float FoodValue;

    public TextMeshProUGUI WaterText;
    public float WaterValue;

    public TextMeshProUGUI PopulationText;
    public float PopulationValue;

    public float foodRate;
    public float waterRate;
    public float populationRate;
    
    #endregion

    void Update ()
    {
        FoodValue = foodRate;
        FoodText.text = "Food: " + FoodValue.ToString("F0");

        WaterValue = waterRate;
        WaterText.text = "Water: " + WaterValue.ToString("F0");

        PopulationValue = populationRate;
        PopulationText.text = "Population: " + PopulationValue.ToString("F0");
    }
}
