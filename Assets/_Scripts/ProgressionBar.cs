﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressionBar : MonoBehaviour {

    public Slider bar;
    
    public float progress;
    public CurrencyScript currency;

	// Use this for initialization
	void Start ()
    {
        bar.maxValue = 50;
	}
	
	// Update is called once per frame
	void Update ()
    {
        progress = currency.FoodValue / currency.PopulationValue;
        bar.value = progress;
		//progress = 
	}
}
